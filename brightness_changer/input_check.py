def raw_cb(raw_str):
    """
    Проверка входных данных.
    """
    accept_alph = '0123456789abcdef'
    try:
        mode, color, bright = raw_str.split()
        bright = int(bright)
    except ValueError:
        print('Параметр яркости должен быть целым числом')
    except AttributeError:
        print('Минимум 3 аргумента')
    else:
        if len(color) == 7:
            color = color[1:]
        color = color.lower()
        if all(i in accept_alph for i in color) and len(color) == 6 and \
                0 <= bright <= 100:
            return mode, color, bright
        else:
            print('Введены некорректные данные')


if __name__ == '__main__':
    print(raw_cb('#000s00 100'))
